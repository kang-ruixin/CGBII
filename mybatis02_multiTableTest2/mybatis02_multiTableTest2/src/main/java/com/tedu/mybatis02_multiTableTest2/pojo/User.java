package com.tedu.mybatis02_multiTableTest2.pojo;
//对应user表
public class User {

	//对应user_id列
	Integer  userId;
	
	//对应 user_name列
	String   userName;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
	
}
