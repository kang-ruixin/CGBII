package com.tedu.mybatis02_multiTableTest2.mapper;

import com.tedu.mybatis02_multiTableTest2.pojo.JTOrder;

public interface JTOrderMapper {

	
	//通过接口操作数据库
	//对象是mybatis动态生成对象
	public   JTOrder    select(Integer orderId);
}
