package com.tedu.mybatis02_multiTableTest2;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.tedu.mybatis02_multiTableTest2.mapper")
public class Mybatis02MultiTableTest2Application {

	public static void main(String[] args) {
		SpringApplication.run(Mybatis02MultiTableTest2Application.class, args);
	}

}
