package org.springmvc;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;


//服务器     相当于  Tomcat
public class WebServer {

	
	public static void main(String[] args) throws Throwable {
		//socket编程  ，线程编程
		//开启服务器，客户端吧信息发给9090端口
		ServerSocket    serverSocket  =new   ServerSocket(9090);
		System.out.println("服务器启动了");
		//不停的监听客户端是否发过来请求
		while(true) {
			//接收客户端发过来的请求
			Socket  socket =serverSocket.accept();
			//没来一个用户启动一个线程
			
			HttpThread  httpThread  =new HttpThread(socket);
			httpThread.start();
		}
	}
	
	
	//内部类
	static   class  HttpThread   extends  Thread{
		Socket  Socket;//代表和客户端连接
		public   HttpThread(Socket  socket) {
			// TODO Auto-generated constructor stub
			this.Socket=socket;
		}
		@Override
		public    void    run() {
			try {
				//接收浏览器发过来的数据
				
				//得到输入流InputStream
				InputStream   inputStream =Socket.getInputStream();
				//把输入流转成InputStreamRead
				InputStreamReader   inputStreamReader =new InputStreamReader(inputStream);
				//把输入流转成BufferedRead
     BufferedReader   bufferedReader  =new  BufferedReader(inputStreamReader);
				
     
     String   requestLine  =bufferedReader.readLine();
     System.out.println(requestLine);
     
     
				//调用dispacherserverlet
				
				//把结果返回给浏览器
			} catch (Exception e) {
				// TODO: handle exception
			}
			super.run();
		}
		
	}
}
