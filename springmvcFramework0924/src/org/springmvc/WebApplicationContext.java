package org.springmvc;

import java.lang.reflect.Method;
import java.util.HashMap;
//放全局数据
//context上下文
public class WebApplicationContext {

	
	public  static HashMap<String,ControllerDefinition>
	urlMapping  =new  HashMap();
	//初始化
    public   void  init()  throws  Throwable{
    	//扫描所有 controller,
    	String[]  controllerNames  = {"com.tedu.mall.UserController"};
    	for (String controllerName : controllerNames) {
    	   //得到controller
			Class clazz = Class.forName(controllerName);
			Method[] methods = clazz.getDeclaredMethods();
			//遍历所有方法
			for (Method method : methods) {
				//判断方法有没有加@requestMapping注解
				RequestMapping requestMapping = method.getAnnotation(RequestMapping.class);
				if (requestMapping != null) {
					//得url
					String url = requestMapping.value();
					String methodName = method.getName();
					//创建controllerDefintion
					boolean isJson = false;

					ControllerDefinition controllerDefinition = new ControllerDefinition(url, controllerName,
							methodName, isJson);
					//把url，controllerDefinition放到hashmap中
					urlMapping.put(url, controllerDefinition);
				}
				}
			}
    	}
    }
			
		
	