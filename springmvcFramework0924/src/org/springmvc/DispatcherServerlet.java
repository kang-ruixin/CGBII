package org.springmvc;
//控制器    收到  webserver的请求，调controller

import java.lang.reflect.Method;

import javax.http.HttpServerletRequest;
import javax.http.HttpServerletResponse;

public class DispatcherServerlet {


		//
		WebApplicationContext webApplicationContext = new WebApplicationContext();

		public  DispatcherServerlet() throws Throwable {
			webApplicationContext.init();
		}
	
	
	////    收到  webserver的请求，调controller
	/**
	 * 
	 * @param request   放的请求
	 * @param url 
	 * @return   controller    执行的结果
	 */
	public    HttpServerletResponse    doDispatch(HttpServerletRequest  request, Object url) {

      //1.从request中得到url
		String  urlString =request.getUrl();
		//2.根据  url
		ControllerDefinition  definition  =webApplicationContext
				.urlMapping.get(url);
		
		//3.从diefinition中得到controllerName
		String    controllerName=definition.getControllerName();
		//4.从difinition得到方法名
		String   methodName =definition.getMethodName();
		
		
		//5.通过反射创建对象
		Class   clazz  =Class.forName(controllerName);
		Object  object = clazz.newInstance();
		
		//6.通过类对象创建method
		Method  method  =clazz.getEnclosingMethod();
		
		//7.通过method.invoke执行方法
		Object  result  =method.invoke(object);
		HttpServerletResponse   response  =(H)request;
		return  response;
	}

}
