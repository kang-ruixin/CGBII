package com.tedu.springmvc08_itemlist.controller;



import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ItemController {

	
	//返回商品列表
	@RequestMapping("/list")
	public  List<Item>  list(){
		ArrayList<Item> items=new ArrayList<Item>();
		items.add(new Item("华为手机", 99999));
		items.add(new Item("小米电视", 88888));
		return  items;
		
	}
}
