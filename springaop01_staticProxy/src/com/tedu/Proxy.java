package com.tedu;
//代理类：的时间后，调用目标类
//代理类中的方法必须和目标类一直
public class Proxy implements  IDAO{

	//
	IDAO targrt;
	//通过构造方法接收一个目标类对象
	public  Proxy (IDAO  target) {
		super();
		this.targrt=target;
	}
	
	
	
	
	@Override
	public void select() {
  //得到其实时间（扩展功能      ，   无侵入式代码实现的扩展功能）
		long   startTime=System.nanoTime();
		//调用目标类.select()
		targrt.select();
		//得结束时间
		long   endTime=System.nanoTime();
		//打印时间
		System.err.println("time="+(endTime-startTime));
		
	}

}
