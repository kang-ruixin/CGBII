package com.tedu;

public class TestStaticProxy {

	public static void main(String[] args) {
		//创建目标类对象
		UserDao  userDao =new UserDao();
		OrderDAO orderDAO =new  OrderDAO();
		//创建代理
		Proxy  userDAOProxy=new Proxy(userDao);
		Proxy   orderDAOProxy= new Proxy(userDao);
		//调用同代理的方法
		userDAOProxy.select();
		orderDAOProxy.select();
		//直接调用目标类
		userDao.select();
		orderDAO.select();
	}
}
