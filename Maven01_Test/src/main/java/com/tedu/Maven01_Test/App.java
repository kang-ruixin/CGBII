package com.tedu.Maven01_Test;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	try {
			//创建一个实体类
    		ItemCategory  itemCategory  =new  ItemCategory();
    		itemCategory.setId(88);
    		itemCategory.setName("手机");
    		//创建一个把实体对象转成json的对象
    		ObjectMapper  objectMapper =new ObjectMapper();
    		
    		//转换
    		String  json=objectMapper.writeValueAsString(itemCategory);
    		//打印json
    		System.out.println(json);
		} catch (Exception e) {
			 e.printStackTrace();
			 //出错发邮件
			 //发短信
		}
        System.out.println( "Hello World!" );
    }
}
