package com.tedu.springmvc11_page.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class UserController {

	//原先登陆地址
	@RequestMapping("/login")
	public  String  login() {
		return "redirect:https://passport.jd.com/new/login.aspx";
	}
	//只接受用户名和密码
	@RequestMapping("/up")
	@ResponseBody
	public  String usernameAndPwd(String username,String pwd) {
		return  username+":"+pwd;
	}
	//接收 用户名和密码和验证码
	@RequestMapping("/code")
	public String  usenameAndPwdAndCode(String username,String pwd,String code) {
		//判断验证码
		//用转发forware调用/up 
		return  "forward:/up";
	}
}
