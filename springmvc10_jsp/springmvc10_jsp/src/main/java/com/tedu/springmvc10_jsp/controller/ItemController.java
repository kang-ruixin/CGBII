package com.tedu.springmvc10_jsp.controller;

import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller  //不是RestController。
//restController返回的是json或字符串
//controller返回jsp
public class ItemController {
 //ModelAndView model 业务View视图界面
 @RequestMapping("/listItem")
 public ModelAndView listItem() {
  //返回web-inf/jsp/index.jsp
  ModelAndView modelAndView=new ModelAndView("index");
  
  ArrayList<Item> itemList=new ArrayList<Item>();
  itemList.add(new Item("华为手机",88888));
  itemList.add(new   Item("小米手机",99999)); 
  modelAndView.addObject("itemList",itemList);
  return modelAndView;
  //关闭以前的程序，debug as启动
  //localhost:1314/listItem
 }
}