package org.springmvc;
//springmvc入口类

import java.lang.reflect.Method;
import java.util.HashMap;



public class AppMain {
//   register   ,controllerDefinition[UserController,register]
    static	HashMap<String, ControllerDefinition>
     urlMapping= new HashMap();
	public static void main(String[] args) throws Throwable {
		//加载类
		loadObjets();
		//处理请求
		RequestProcess("/register");
		RequestProcess("/login");
	}
	private static void RequestProcess(String url) throws Throwable {
		//从urlMapping找到一个controllerDefinition
		ControllerDefinition definition=urlMapping.get(url);
		//得到类名，方法名
		String className=definition.getClassName();
		String methodName=definition.getMethodName();
		//根据类名得到类对象
		Class  clazz=Class.forName(className);

		Object object =clazz.newInstance();
		//根据方法名称，得到一个方法
		Method  method =clazz.getDeclaredMethod(methodName);
		//调用这个方法
		Object result =method.invoke(object);
		System.out.println(url+":"+result);
	}
	private static void loadObjets() throws Throwable {
		String  className="com.tedu.UserController";
		//得到类对象
		Class clazz=Class.forName(className);
		//判断有没有加@restController
		RestController  restController=(RestController)
				clazz.getAnnotation(RestController.class);
		if (restController!=null) {
			
		
		//得到所有方法
		Method[]  methods=clazz.getDeclaredMethods();
		//遍历所有方法
		for (Method method:methods) {
			
			//判断方法有没有加@requestMapping
			RequestMapping  requestMapping =method.getAnnotation
					(RequestMapping.class);
			if (requestMapping!=null) {
			//如果加上，得url
				String url=requestMapping.value();
			//创建controllerDefinition
				String methodName=method.getName();
				ControllerDefinition definition=new ControllerDefinition(url, className, methodName);
			//把url，controllerDefinition放到hashmap中
				urlMapping.put(url,definition);		
						
			}
		}
		
		}
	}
}
