package com.tedu.sp02_mail.controller;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController//框架自动创建对象
public class MailController {
	  //发邮件的对象
	//以前都是通过new来创建对象
	//让框架给我们一个对象
	@Autowired
	JavaMailSender   javaMailSender;
	public void  SendMail(String subject,String text) {
		//创建邮件
		SimpleMailMessage  mailMessage =new   SimpleMailMessage();
		//设置收件人
		mailMessage.setTo("kangruixin826@qq.com");
		
		//是指发件.人
		mailMessage.setFrom("kangruixin826@qq.com");
		//设置邮件标题
		mailMessage.setSubject(subject);
		//是指邮件正文
		mailMessage.setText(text);
		//发送邮件
		javaMailSender.send(mailMessage); 
	}
	@RequestMapping("/send1")
	public String send1() {
		SendMail("明天入职", "明天入职");
		return "成功";
	}
	@RequestMapping("/send2")
    public  String send2() {
    	try {
			int n=10/0;
		} catch (Exception e) {
			// 程序员看不到异常信息
			StringWriter  stringWriter =new StringWriter();
			PrintWriter   printWriter  =new  PrintWriter(stringWriter);
			//异常信息输出到printwrite，输出到了stringwrite
			e.printStackTrace(printWriter);
			SendMail("异常信息", stringWriter.toString());
			
		}
    	return "收邮件";
    }
}
