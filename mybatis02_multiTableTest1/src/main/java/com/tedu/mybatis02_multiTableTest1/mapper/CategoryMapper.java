package com.tedu.mybatis02_multiTableTest1.mapper;
//操作category表
import com.tedu.mybatis02_multiTableTest1.pojo.Category;

public interface CategoryMapper {
//查询分类和  分类下的商品
	public   Category    selectCategory(Integer  categoryId);
}
