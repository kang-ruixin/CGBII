package com.tedu.mybatis02_multiTableTest1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tedu.mybatis02_multiTableTest1.mapper.CategoryMapper;
import com.tedu.mybatis02_multiTableTest1.pojo.Category;

@RestController
public class CategoryController {
	@Autowired
	CategoryMapper   categoryMapper;
	
	@RequestMapping("/cat")
	public  Category    selectCategory() {
		return   categoryMapper.selectCategory(2);
	}

}
