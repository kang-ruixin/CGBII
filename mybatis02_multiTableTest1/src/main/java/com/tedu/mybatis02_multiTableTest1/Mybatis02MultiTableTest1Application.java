package com.tedu.mybatis02_multiTableTest1;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
//在Application中加上@mapperscan告诉mybatis框架接口的包
@MapperScan("com.tedu.mybatis02_multiTableTest1.mapper")
public class Mybatis02MultiTableTest1Application {

	public static void main(String[] args) {
		SpringApplication.run(Mybatis02MultiTableTest1Application.class, args);
	}

}
