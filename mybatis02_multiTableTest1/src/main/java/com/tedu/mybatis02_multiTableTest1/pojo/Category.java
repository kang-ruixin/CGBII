package com.tedu.mybatis02_multiTableTest1.pojo;
//对应  category表

import java.util.List;

public class Category {

	//对应category_id列
	Integer   categoryId;
	//对应category_name列
	String    categoryName;
	
	List<Item>  itemList;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<Item> getItemList() {
		return itemList;
	}

	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}
	
}
