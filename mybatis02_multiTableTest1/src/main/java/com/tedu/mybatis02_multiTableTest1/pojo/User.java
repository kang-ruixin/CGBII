package com.tedu.mybatis02_multiTableTest1.pojo;
//对应user表
public class User {

	//对应user_id列
	Integer   userId;
	
	//对应 user_name列
	String   username;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}
