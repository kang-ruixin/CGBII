package com.tedu.springmvc01_base.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

	@RequestMapping("/register1")
	
	public   String   register1(String  username,String password) {
		return "username="+username+",password="+password;
	}
	//給方法傳參數，參數很多。創建一個類
	//http://localhost:1314?username=root&password=root
	@RequestMapping("/register2")
	public  String  register2(User user) {
		return"register2 username="+user.getUsename()
		+"password="+user.getPassword();
	}
	@RequestMapping("/register3")
	public  String  register3(HttpServletRequest request) {
		String name =request.getParameter("username");
		String password =request.getParameter("password");
		String ip=request.getRemoteAddr();
		return "regeister3 name="+name+",password="+password+",ip="+ip;
		
	}
	@RequestMapping("/register4")
	public  void  register4(HttpServletRequest request,HttpServletResponse response) throws Exception {
		String name =request.getParameter("username");
		String password =request.getParameter("password");
		String ip=request.getRemoteAddr();
		String result= "regeister3 name="+name+",password="+password+",ip="+ip;
		
		//用resp[onse返回結果
		//告訴瀏覽器返回的是html
		response.setContentType("text/html");
		//設置字符集
		response.setCharacterEncoding("UTF-8");
		//返回内容
		PrintWriter printWriter=response.getWriter();
		printWriter.println(request);
	}
	
	}
