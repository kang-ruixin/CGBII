package com.tedu.springmvc01_base.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ItemController {
        
	@RequestMapping("delete1")
	  public  String delete1(Integer id) {
		  return "id="+id;
	  }
	
	
	@RequestMapping("delete2")
	public  String delete2(int id) {
		return "id="+id+",this"+this;
	}
}
