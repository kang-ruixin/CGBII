package org.springmvc;

import com.tedu.UserController;

public class Main {

	public static void main(String[] args) {
		//判断类有没有加RestController
		//.class返回一个对象    类对象
		//new  ArrayList  数组集合
		//类对象     访问私有属性 ，执行方法，判断类有没有加注解
		Class clzz= UserController.class;
		//如果加上自动创建对象
		RestController  restController =(RestController) clzz.
				getAnnotation(RestController.class);
		
		if (restController!=null) {
			//加了注解
			UserController  userController =new UserController();
			System.out.println(userController);
			
		}
	}
}
