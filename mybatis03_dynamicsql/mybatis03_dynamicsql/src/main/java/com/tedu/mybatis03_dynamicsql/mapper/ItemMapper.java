package com.tedu.mybatis03_dynamicsql.mapper;
//操作item表

import java.util.List;

import com.tedu.mybatis03_dynamicsql.pojo.Item;

public interface ItemMapper {
	
	
  //返回多个数据
	public    List<Item>    select(Item   item);
	
	//查询多个商品
	public  List<Item>  list(List<Integer>  idList);
}
