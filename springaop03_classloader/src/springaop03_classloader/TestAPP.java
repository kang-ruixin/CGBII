package springaop03_classloader;

import java.io.IOException;
import java.io.InputStream;

public class TestAPP {

	public static void main(String[] args) throws Throwable {
		  //TestAPP必须由类加载器把类的方法加载到方法区
		//创建一个对象
		TestAPP  testAPP  =new   TestAPP();
		//得到类对象
		Class   clazz =testAPP.getClass();
		//根据类对象得到类加载器
		ClassLoader   classLoader= clazz.getClassLoader();
		System.out.println(classLoader);
		
		//读取文件用java.io.File(d:\*\config.txt)
		
		//通过类加载器来读取文件，得到输入流
		InputStream  inputStream  =classLoader.getResourceAsStream("config.txt");
		//得到流的长度
		int  length =inputStream.available();
		//创建一个byte[]
		byte[]  data=new byte[length];
		//把输入流的内容督导byte[]
		inputStream.read(data);
		//把byte[]转成字符串
		String proxyName=new String(data);
		System.out.println(proxyName);
		
		
		//使用类加载器加载一个类
		Class  proxyClazz  =classLoader.loadClass(proxyName);
		Object   proxyObject= proxyClazz.newInstance();
		System.out.println(proxyObject);
	}
}
