package com.tedu.springion01_userService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springion01UserServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(Springion01UserServiceApplication.class, args);
	}

}
