package com.tedu.sp01_test.contorller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//控制器
//浏览器-->控制器-->业务service
//让框架去创建对象
@RestController
public class CategoryController {
	// 通过url地址访问java中的方法
	// 设置地址request请求map,hashpam<key,value>key-->value
	// http://localhost:8080/test
	@RequestMapping("/test")
	public ItemCategory select() {
		ItemCategory itemCategory = new ItemCategory();
		itemCategory.setId(88);
		itemCategory.setName("手机");
		return itemCategory;// 框架自动把对象转成json
	}
}
