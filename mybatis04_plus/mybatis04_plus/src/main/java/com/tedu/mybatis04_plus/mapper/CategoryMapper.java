package com.tedu.mybatis04_plus.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.tedu.mybatis04_plus.pojo.Category;


//以前的    CRUD  现在不用自己写了
public interface CategoryMapper   extends  BaseMapper<Category>{

}
