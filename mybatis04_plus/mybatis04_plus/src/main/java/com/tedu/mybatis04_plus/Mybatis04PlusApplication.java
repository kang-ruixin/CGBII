package com.tedu.mybatis04_plus;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.tedu.mybatis04_plus.pojo.User;

@SpringBootApplication
@MapperScan("com.tedu.mybatis04_plus.mapper")
public class Mybatis04PlusApplication {

	public static void main(String[] args) {
		SpringApplication.run(Mybatis04PlusApplication.class, args);
		
		
		//测试username有没有  set   get
		User   user=new   User();
		user.setUserId(6688);
		
		System.out.println(user);
	
	}

}
