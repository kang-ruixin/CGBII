package com.tedu.mybatis04_plus.pojo;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableName;

@TableName("category")//实体类对应的表
public class Category {


	@TableField("category_id")//属性对应的列名
	Integer   categoryId;
	
	@TableField("category_name")//属性对应的列名
	String      categoryName;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
	
}
