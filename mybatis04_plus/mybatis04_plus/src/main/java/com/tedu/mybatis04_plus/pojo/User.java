package com.tedu.mybatis04_plus.pojo;

import lombok.Data;

//@Data//lombok 会为User生成set，get方法
public class User {

	
	Integer   userId;

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
}
