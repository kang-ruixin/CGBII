package com.tedu.mybatis01_puls;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Mybatis01PulsApplication {

	public static void main(String[] args) {
		SpringApplication.run(Mybatis01PulsApplication.class, args);
	}

}
