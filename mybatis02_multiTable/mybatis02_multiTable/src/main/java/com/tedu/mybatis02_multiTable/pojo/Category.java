package com.tedu.mybatis02_multiTable.pojo;

import java.util.List;

//对应Category表
public class Category {
   //对应Category_id列
	Integer  categoryId;
	//对应Category_name列
	String categoryName;
	
	List<Item>  itemList;
	
	public List<Item> getItemList() {
		return itemList;
	}
	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getCategoryName() {
		return categoryName;
	}
	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}
	
}
