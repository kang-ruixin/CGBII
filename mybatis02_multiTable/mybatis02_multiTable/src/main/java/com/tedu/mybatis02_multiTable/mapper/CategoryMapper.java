package com.tedu.mybatis02_multiTable.mapper;

import com.tedu.mybatis02_multiTable.pojo.Category;

//操作category表
public interface CategoryMapper {
  //查询分类和分类下的商品
	public   Category   selectCategory(Integer  categoryId);
}
