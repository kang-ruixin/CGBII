package com.tedu.mybatis02_multiTable.pojo;
//对应item表
public class Item {
//对应category_id列
	Integer  catrgoryId;
	//对应item_name列
	String   itemName;
	public Integer getCatrgoryId() {
		return catrgoryId;
	}
	public void setCatrgoryId(Integer catrgoryId) {
		this.catrgoryId = catrgoryId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	
}
