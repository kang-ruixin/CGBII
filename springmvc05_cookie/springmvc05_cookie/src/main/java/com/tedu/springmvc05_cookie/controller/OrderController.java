package com.tedu.springmvc05_cookie.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderController {
    @RequestMapping("/listOrder")
	public  String listOrder(HttpServletRequest request) {
    
    	Cookie[] cookies =request.getCookies();
    	String  string ="";
    	if (cookies!=null) {
			 for (Cookie  cookie:cookies) {
				 String cookieName=cookie.getName();
				 String  cookieValue=cookie.getValue();
				 string =string+cookieName+"="+cookieValue;
				
			}
		}
    	return string;
    }
}
