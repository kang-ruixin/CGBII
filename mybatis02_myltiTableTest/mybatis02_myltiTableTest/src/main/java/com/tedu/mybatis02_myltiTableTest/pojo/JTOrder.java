package com.tedu.mybatis02_myltiTableTest.pojo;
//对应jt_order表
public class JTOrder {

	//order_id列
	Integer   orderId;
	//user_id 列
	Integer   userId;
	public Integer getOrderId() {
		return orderId;
	}
	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	
}
