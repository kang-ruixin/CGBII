package com.tedu.mybatis02_myltiTableTest.mapper;

import com.tedu.mybatis02_myltiTableTest.pojo.JTOrder;

//通过接口操作数据库
//对象是由mybatis动态生成的代理对象

public interface JTOrderMapper {
  //查询订单和用户信息
	public   JTOrder select(Integer  orderId);
	
}
