package com.tedu.mybatis02_myltiTableTest.pojo;
//对应Category表

import java.util.List;

public class Category {
    //category_id列
	Integer  categoryId;
	//category_name列
	String    categoryName;
	
	List<Item>   itemList;

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public List<Item> getItemList() {
		return itemList;
	}

	public void setItemList(List<Item> itemList) {
		this.itemList = itemList;
	}
	
	
	
}
