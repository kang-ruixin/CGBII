package com.tedu.mybatis02_myltiTableTest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.tedu.mybatis02_myltiTableTest.mapper.CategoryMapper;
import com.tedu.mybatis02_myltiTableTest.pojo.Category;

@RestController
public class CategoryController {

	@Autowired
	public CategoryMapper categoryMapper;
	
	@RequestMapping("/cat")
	public  Category   cat() {
		return  categoryMapper.selectCategory(2);
	}
}
