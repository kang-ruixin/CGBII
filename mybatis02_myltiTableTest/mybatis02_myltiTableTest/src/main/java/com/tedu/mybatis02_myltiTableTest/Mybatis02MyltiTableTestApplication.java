package com.tedu.mybatis02_myltiTableTest;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.tedu.mybatis02_myltiTableTest.mapper")
public class Mybatis02MyltiTableTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(Mybatis02MyltiTableTestApplication.class, args);
	}

}
