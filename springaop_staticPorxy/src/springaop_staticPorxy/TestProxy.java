package springaop_staticPorxy;

public class TestProxy  {
   
	public static void main(String[] args) {
		//创建目标对象
		IDAO  userDAO =new UserDAO();
		//得到代理对象
		Proxy userDAOProxy=new Proxy(userDAO);
		//调用代理的方法
		userDAOProxy.select();
		
		IDAO  orderDAO=new  OrderDAO();
		Proxy  orderDAOProxy=new  Proxy(orderDAO);
		orderDAOProxy.select();
	}

}
