package springaop_staticPorxy;

import java.lang.annotation.Target;

public class Proxy   implements IDAO{

	IDAO target;
	 public Proxy(IDAO target) {
     this.target=target;
	 
	 }
	
	
	@Override
	public void select() {
     long  startTime=System.nanoTime();
		target.select();
		long endTime=System.nanoTime();
		System.out.println("select占用的时间="+(endTime-startTime));
		
	}

	
}
