package com.tedu.springaop06_aop.service;

import org.springframework.stereotype.Service;

@Service//让spring创建一个对象，对象放在spring容器中
public class UserServiceImpl   implements UserService{

	@Override
	public String register(String username) {
		return username+"注册成功了";
	}

	
}
