package com.tedu.springaop06_aop.service;

import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

//切面类:执行扩展功能，实现无侵入式编码
@Component  //框架自动创建对象
@Aspect   //这个类是个切面类，在执行业务层方法之前或之后执行切面
public class TimeAspect {
	long   startTime;//开始执行时间
	//切面像springmvc中的interceptor
	//设置在运行那些业务层类时执行TimeAspect
	/**
	 * 第一个*代表类                                                          *                   *
	 * 第二个*代表类中的所有方法                                       *               *
	 * (..)代表方法的参数可以是任何类型                             *           * 
	 *                                                *      *
	 *public  *代表方法返回类型式任何类型                                    *
	 */
	@Pointcut("execution(public * com.tedu.springaop06_aop.service.*.*(..))")
//切入点   ，设置timeAspect执行的时期
	public void   aopPointCut() {
		
	}
	//得到起始时间
	@Before("aopPointCut()")//在目标方法register之前执行
	public  void    getStatrTime() {
		startTime=System.nanoTime();
	}
	
	//得到结束时间
@After("aopPointCut()") //在目标方法register之后执行
	public    void   getEndTime() {
	  long endTime =System.nanoTime();
	  System.out.println("业务层方法执行的时间"+(endTime-startTime));
}
}
