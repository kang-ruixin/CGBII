package com.tedu.springmvc05_cookie.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	//接受用户名，保存用户名到浏览器中
	@RequestMapping("/login")
	public   String  login(String username,HttpServletResponse response) {
	
	//创建cookie
	Cookie  cookie =new Cookie("username", username);
	
	//response把cookie返回给浏览器
	response.addCookie(cookie);
	return "保存cookie";
}
}