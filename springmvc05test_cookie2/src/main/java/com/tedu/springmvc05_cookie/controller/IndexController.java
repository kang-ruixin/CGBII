package com.tedu.springmvc05_cookie.controller;

import javax.servlet.http.Cookie;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

	//保存一个cookie，设置有效期，浏览器关闭还在
	@RequestMapping("/index")
	public   String  indexString (HttpServletResponse response) {
		Cookie  cookie=new Cookie("bd_id","00001");
		cookie.setMaxAge(60*3);
		response.addCookie(cookie);
		return "设置有效期";
	}
}
