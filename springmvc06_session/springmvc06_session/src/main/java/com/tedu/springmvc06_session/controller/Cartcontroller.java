package com.tedu.springmvc06_session.controller;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Cartcontroller {

	//添加商品
	@RequestMapping("/insert")
	public  String insert(String itemName,HttpSession session) {
		//session相当于数据库，保存商品信息，内部用的是hashmap
		//从session中拿信息
		ArrayList<String>	cart=(ArrayList<String>)
				session.getAttribute("cart");
//第一次访问，得到的cart是null
		if (cart==null) {
			cart=new ArrayList<String>();
			//把cart放到session
			session.setAttribute("cart", cart);
		}

		//把商品放入购物车
		cart.add(itemName);
		return"添加成功";
	}
		//显示商品
		@RequestMapping("/list")
		public  String list(HttpServletRequest request,HttpSession session) {
			//从session中取购物车
			ArrayList<String>  cart=(ArrayList<String>)session.getAttribute("cart");
			String string ="";
			for (String  itemName:cart) {
				string=string+itemName;
			}
			return string;
		}
	}
